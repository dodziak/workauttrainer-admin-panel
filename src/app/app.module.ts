import { TrainingsComponent } from './components/main/trainings/trainings.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { DashboardComponent } from './components/main/dashboard/dashboard.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { AuthComponent } from './components/auth/auth.component';
import { MainComponent } from './components/main/main.component';
import { CategoriesComponent } from './components/main/categories/categories.component';
import { ExercisesComponent } from './components/main/exercises/exercises.component';
import { NewCategoryComponent } from './components/main/categories/new-category/new-category.component';
import { CategoryListComponent } from './components/main/categories/category-list/category-list.component';
import { ExercisesListComponent } from './components/main/exercises/exercises-list/exercises-list.component';
import { NewExerciseComponent } from './components/main/exercises/new-exercise/new-exercise.component';
import { TrainingsListComponent } from './components/main/trainings/trainings-list/trainings-list.component';
import { NewTrainingComponent } from './components/main/trainings/new-training/new-training.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent,
    AuthComponent,
    MainComponent,
    TrainingsComponent,
    CategoriesComponent,
    ExercisesComponent,
    NewCategoryComponent,
    CategoryListComponent,
    ExercisesListComponent,
    NewExerciseComponent,
    TrainingsListComponent,
    NewTrainingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    JwtModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
