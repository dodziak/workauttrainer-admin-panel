// login
export interface JwtLoginToken {
    userId: string;
    authenticated: boolean;
    token: string;
    expiresOn: string;
}
// category
export interface AddCategoryModel {
    name: string;
    categoryType: number;
}
export interface CreateCategoryResponseModel {
    id: string;
}

export interface GetCategory {
    maxPage: number;
    page: number;
    results: CategoryResult[];
}

export interface CategoryResult {
    id: string;
    name: string;
}

// exercise
export interface Exercise {
    maxPage: number;
    page: number;
    results: ExerciseResult[];
}

export interface ExerciseResult {
    id: string;
    name: string;
    reps: number;
    series: number;
    break: number;
    user?: any;
    categories: ExerciseCategory[];
}

export interface ExerciseCategory {
    name: string;
    categoryType: number;
    stringCategory: string;
    id: string;
}

export interface ExeRequestModel {
    name: string;
    reps: number;
    series: number;
    break: number;
    exerciseCategoryId: string[];
  }

// trainings
export interface Training {
    maxPage: number;
    page: number;
    results: TrainingResult[];
}

export interface TrainingResult {
    id: string;
    name: string;
    user: User;
    categories: TrainingCategory[];
    isCollapsed: any; // do listy collapsowanej, nie dostaje z backendu
}

export interface User {
    email: string;
    id: string;
}

export interface TrainingCategory {
    name: string;
    categoryType: number;
    stringCategory: string;
    id: string;
}

export interface SuperSeries {
    id: string;
    reps: number;
    series: number;
    break: number;
    exerciseEndSeriesType: number;
}

export interface TrainingExercise {
    no: number;
    id: string;
    reps: number;
    series: number;
    break: number;
    exerciseEndSeriesType: number;
    superSeries: SuperSeries;
}

export interface CreateTrainingModel {
    name: string;
    exercises: TrainingExercise[];
}

