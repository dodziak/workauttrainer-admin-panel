import { JwtLoginToken } from './../utils/Models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  apiURL = 'https://api.workaut-trainer.jorm.dev/api';
  private userId: string;
  private authenticated: boolean;
  private token: string;
  private expiresOn: Date;

  public get jwtToken() {
    return this.token;
  }


  public get loggedIn(): boolean {
    return this.authenticated && this.token && new Date() < this.expiresOn;
  }

  constructor(private http: HttpClient) {
    // zachowanie tokena przy refreshu
    const objToken = JSON.parse(localStorage.getItem('access_token')) as JwtLoginToken;
    if (objToken != null) {
      this.authenticated = objToken.authenticated;
      this.expiresOn = new Date(objToken.expiresOn);
      this.token = objToken.token;
    }

   }

  login(email: string, password: string): Promise<JwtLoginToken> {
    const request = this.http.post<JwtLoginToken>(`${this.apiURL}/auth/get-token`, { email, password }).toPromise();
    request.then(res => {
      this.userId = res.userId;
      this.authenticated = res.authenticated;
      this.token = res.token;
      this.expiresOn = new Date(res.expiresOn);

      console.log('Jwt service', res);
      localStorage.setItem('access_token', JSON.stringify(res));

      return res;
    });
    return request;
  }

  register(email: string, password: string, confirmPassword: string) {
    return this.http.post(`${this.apiURL}/auth/email-register`, { email, password, confirmPassword });
  }

  logout() {
    localStorage.removeItem('access_token');
    this.token = null;
    this.authenticated = false;
    this.expiresOn = null;
  }

}
