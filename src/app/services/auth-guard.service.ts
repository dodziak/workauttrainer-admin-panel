import { Injectable } from '@angular/core';
import { CanActivate, Router, CanActivateChild } from '@angular/router';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public jwtSvc: JwtService, public router: Router) { }

  canActivate(): boolean {
    if (!this.jwtSvc.loggedIn) {
      this.jwtSvc.logout();
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
