import { JwtLoginToken, GetCategory, CreateCategoryResponseModel } from './../utils/Models';
import { JwtService } from './jwt.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CategoryResult } from '../utils/Models';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  myPage = 1;
  categoriesList: CategoryResult[];
  maxPage: number;
  currentPage = 1;


  pageList: number[];

  constructor(private http: HttpClient, private jwtSvc: JwtService) { }

  private getOptions() {
    const options = {};
    options['headers'] = new HttpHeaders({ Authorization: 'Bearer ' + this.jwtSvc.jwtToken });
    return options;
  }

  getCategories(): Promise<GetCategory> {
    // tslint:disable-next-line:max-line-length
    const request = this.http.get<GetCategory>(`https://api.workaut-trainer.jorm.dev/admin/categories/category?Page=${this.myPage}&CountPerPage=12&Model.SearchString=%20`, this.getOptions())
      .toPromise();
    request.then(res => {
      this.myPage = res.page;
      this.maxPage = res.maxPage;
      this.categoriesList = res.results;
      // inicjacja tablicy ze stronami
      this.pageList = Array.from(Array(res.maxPage), (x, index) => index + 1);
      return res;
    });
    return request;
  }

  addCategory(name: string, categoryType: number) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<CreateCategoryResponseModel>(`https://api.workaut-trainer.jorm.dev/admin/categories/category`, { name, categoryType }, this.getOptions())
      .subscribe(res => {
        console.log('Pomyślnie dodano: ', res);
      });
  }

  deleteCategory(categoryId: string) {
    // tslint:disable-next-line:max-line-length
    return this.http.delete(`https://api.workaut-trainer.jorm.dev/admin/categories/category/` + categoryId, this.getOptions())
      .subscribe(res => {
        console.log('Pomyślnie usunięto:');
      });
  }

  changePage(page: number) {
    this.myPage = page;
    this.getCategories();
    this.currentPage = page;
  }
}



