import { ExerciseCategory, ExeRequestModel } from './../utils/Models';
import { JwtService } from 'src/app/services/jwt.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ExerciseResult, Exercise } from '../utils/Models';

@Injectable({
  providedIn: 'root'
})
export class ExercisesService {
  myPage = 1;
  exercisesList: ExerciseResult[];
  maxPage: number;
  currentPage = 1;

  pageList: number[];

  constructor(private http: HttpClient, private jwtSvc: JwtService) { }

  private getOptions() {
    const options = {};
    options['headers'] = new HttpHeaders({ Authorization: 'Bearer ' + this.jwtSvc.jwtToken});
    return options;
  }

  getExercises(): Promise<Exercise> {
    // tslint:disable-next-line:max-line-length
    const request = this.http.get<Exercise>(`https://api.workaut-trainer.jorm.dev/admin/templates/exercise?Page=${this.myPage}&CountPerPage=20&SortType=0&Model.SortBy=0`, this.getOptions())
      .toPromise();
    request.then(res => {
        this.myPage = res.page;
        this.maxPage = res.maxPage;
        this.exercisesList = res.results;

        // inicjacja tablicy ze stronami
        this.pageList = Array.from(Array(res.maxPage), (x, index) => index + 1);
        console.log(res.results);
        return res;
      });
    return request;
  }

  getAllExercises(): Promise<Exercise> {
    // tslint:disable-next-line:max-line-length
    const allRequest = this.http.get<Exercise>(`https://api.workaut-trainer.jorm.dev/admin/templates/exercise?Page=1&CountPerPage=100&SortType=0&Model.SortBy=0`, this.getOptions())
    .toPromise();
    allRequest.then(res => {
        this.exercisesList = res.results;
        return res;
      });
    return allRequest;
  }

  addExercise(newExercise: ExeRequestModel) {
    // tslint:disable-next-line:max-line-length
    return this.http.post(`https://api.workaut-trainer.jorm.dev/admin/templates/exercise`, newExercise, this.getOptions())
    .subscribe(res => {
      console.log('Pomyślnie dodano ćwiczenie: ', res);
    });
  }

  deleteExercise(id: string) {
    return this.http.delete(`uzupełnić` + id, this.getOptions())
    .subscribe(res => {
      console.log('Pomyślnie usunięto ćwiczenie: ', res);
    })
  }

  changePage(page: number) {
    this.myPage = page;
    this.getExercises();
    this.currentPage = page;
  }

}
