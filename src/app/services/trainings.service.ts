import { Training, CreateTrainingModel } from './../utils/Models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtService } from 'src/app/services/jwt.service';
import { Injectable } from '@angular/core';
import { TrainingResult } from '../utils/Models';

@Injectable({
  providedIn: 'root'
})
export class TrainingsService {

  myPage = 1;
  trainingsList: TrainingResult[];
  maxPage: number;
  currentPage = 1;
  pageList: number[];


  constructor(private jwtSvc: JwtService, private http: HttpClient) { }

  private getOptions() {
    const options = {};
    options['headers'] = new HttpHeaders({ Authorization: 'Bearer ' + this.jwtSvc.jwtToken});
    return options;
  }

  getTrainings() {
    // tslint:disable-next-line:max-line-length
    return this.http.get<Training>(`https://api.workaut-trainer.jorm.dev/admin/templates/training?Page=${this.myPage}&CountPerPage=5&SortType=0&Model.SortBy=0`, this.getOptions())
      .subscribe(res => {
        this.myPage = res.page;
        this.maxPage = res.maxPage;
        this.trainingsList = res.results;

        console.log('trening serwis', res.results);
        // inicjacja tablicy ze stronami
        this.pageList = Array.from(Array(res.maxPage), (x, index) => index + 1);
      });
  }

  addTraining(newTraining: CreateTrainingModel) {
    return this.http.post(`https://api.workaut-trainer.jorm.dev/admin/templates/training`, newTraining, this.getOptions() )
    .subscribe(res => {
      console.log('Pomyślnie dodano trening: ', res);
    });
  }

  changePage(page: number) {
    this.myPage = page;
    this.getTrainings();
    this.currentPage = page;
  }
}
