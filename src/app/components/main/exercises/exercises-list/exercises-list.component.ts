import { Component, OnInit } from '@angular/core';
import { ExercisesService } from 'src/app/services/exercises.service';

@Component({
  selector: 'app-exercises-list',
  templateUrl: './exercises-list.component.html',
  styleUrls: ['./exercises-list.component.scss']
})
export class ExercisesListComponent implements OnInit {

  constructor(public exeSvc: ExercisesService) { }

  ngOnInit() {
    this.getExercises();
  }

  getExercises() {
    return this.exeSvc.getExercises();
  }

  deleteExercise(id) {
    return this.exeSvc.deleteExercise(id);
  }

  changePage(page: number) {
    this.exeSvc.changePage(page);
  }
}
