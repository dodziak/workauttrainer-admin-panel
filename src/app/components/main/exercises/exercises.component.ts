import { ExercisesService } from './../../../services/exercises.service';
import { ExerciseCategory, Exercise } from './../../../utils/Models';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ExerciseResult } from 'src/app/utils/Models';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss']
})
export class ExercisesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
