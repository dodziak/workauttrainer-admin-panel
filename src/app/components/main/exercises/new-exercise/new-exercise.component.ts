import { NgSelectModule, NgOption, NgSelectConfig } from '@ng-select/ng-select';
import { CategoryResult, ExeRequestModel } from './../../../../utils/Models';
import { ExercisesService } from './../../../../services/exercises.service';
import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-new-exercise',
  templateUrl: './new-exercise.component.html',
  styleUrls: ['./new-exercise.component.scss']
})
export class NewExerciseComponent implements OnInit {
  addExerciseForm: FormGroup;
  submitted = false;
  categoryArr: CategoryResult[] = [];
  requestModel: ExeRequestModel;

  // łatwy dostep do pól formularza
  get fControls() { return this.addExerciseForm.controls; }

  constructor(
    private exeSvc: ExercisesService,
    private catSvc: CategoriesService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.catSvc.getCategories().then(res => {
      this.categoryArr = res.results;
    });

    this.addExerciseForm = this.formBuilder.group({
      name: ['', Validators.required],
      reps: [0, Validators.required],
      series: [0, Validators.required],
      break: [0, Validators.required],
      exerciseCategoryId: new FormControl([])
    });
  }

  onSubmit() {
    this.requestModel = this.addExerciseForm.value;

    this.submitted = true;
    console.log(
      'reps: ', typeof (this.fControls.reps.value),
      'series: ', typeof (this.fControls.series.value),
      'break: ', typeof (this.fControls.break.value),
      'categoryid: ', typeof (this.fControls.exerciseCategoryId.value),
      'categoryid value: ', this.fControls.exerciseCategoryId.value);

    if (this.addExerciseForm.invalid) {
      return;
    }
    console.log(this.requestModel);
    // tslint:disable-next-line:max-line-length
    this.exeSvc.addExercise(this.requestModel);
  }
}

