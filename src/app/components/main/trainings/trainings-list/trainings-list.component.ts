import { Component, OnInit } from '@angular/core';
import { TrainingsService } from 'src/app/services/trainings.service';

@Component({
  selector: 'app-trainings-list',
  templateUrl: './trainings-list.component.html',
  styleUrls: ['./trainings-list.component.scss']
})
export class TrainingsListComponent implements OnInit {
  isCollapsed = false;

  constructor(public trainingSvc: TrainingsService) { }

  ngOnInit() {
    this.getTrainings();
  }

  getTrainings() {
    return this.trainingSvc.getTrainings();
  }

  changePage(page: number) {
    this.trainingSvc.changePage(page);
  }

}
