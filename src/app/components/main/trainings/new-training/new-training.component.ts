import { ExerciseResult } from './../../../../utils/Models';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { ExercisesService } from 'src/app/services/exercises.service';
import { TrainingsService } from './../../../../services/trainings.service';
import { Component, OnInit } from '@angular/core';
import { CreateTrainingModel, TrainingExercise, SuperSeries } from 'src/app/utils/Models';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.scss']
})
export class NewTrainingComponent implements OnInit {

  trainingRequestModel: CreateTrainingModel;
  addTrainingForm: FormGroup;
  submitted = false;
  exercises: FormArray;
  exerciseList: ExerciseResult[] = [];
  no: any = 2;
  endTypeList: number[] = [0, 1, 2];

  get exeControls() { return this.addTrainingForm.get('exercises') as FormArray; }

  constructor(
    public trngSvc: TrainingsService,
    public exeSvc: ExercisesService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.exeSvc.getAllExercises().then(res => {
      this.exerciseList = res.results;
    });

    this.addTrainingForm = this.formBuilder.group ({
      name: this.formBuilder.control(['']),
      exercises: this.formBuilder.array([this.newExercise()])
    });
  }

  newExercise(): FormGroup {
    const ex = this.formBuilder.group({
      no: 1,
      id: '',
      reps: 0,
      series: 0,
      break: 0,
      exerciseEndSeriesType: 0,
      superSeries: {
        id: 0,
        reps: 0,
        series: 0,
        break: 0,
        exerciseEndSeriesType: 0
      }
    });

    ex.get('id').valueChanges.subscribe(x =>{
      console.log('new exe', x);

      // przypisanie zmiennych do ćwiczenia o danym id
      const exercisesDetails = this.exerciseList.find(item => item.id == x);

      if (exercisesDetails === null) {
        return ;
      }

      ex.get('reps').setValue(exercisesDetails.reps);
      ex.get('series').setValue(exercisesDetails.series);
      ex.get('break').setValue(exercisesDetails.reps);
      ex.get('superSeries').setValue(null);
    });
    return ex;
  }

  addExercise(): void {
    this.exercises = this.addTrainingForm.get('exercises') as FormArray;

    // inkrementacja 'no'
    const newExercise = this.newExercise();
    newExercise.get('no').setValue(this.no);
    this.no += 1;

    this.exercises.push(newExercise);
  }

  removeExercise(index: number) {
    this.exercises = this.addTrainingForm.get('exercises') as FormArray;

    this.exercises.removeAt(index);
    console.log('deleted index', index);
 }

  onSubmit() {
    this.trainingRequestModel = this.addTrainingForm.value;
    this.submitted = true;

    console.log('form value: ', this.addTrainingForm.value);

    if (this.addTrainingForm.invalid) {
      alert('Niepoprawny formularz !');
      return;
    }

    this.trngSvc.addTraining(this.trainingRequestModel);
  }
}
