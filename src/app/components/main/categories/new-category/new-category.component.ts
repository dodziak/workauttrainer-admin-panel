import { CategoriesService } from './../../../../services/categories.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.scss']
})
export class NewCategoryComponent implements OnInit {
  addCategoryForm: FormGroup;
  submitted = false;

  // łatwy dostep do pól formularza
  get fControls() { return this.addCategoryForm.controls; }

  constructor(private catSvc: CategoriesService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.addCategoryForm = this.formBuilder.group({
      name: ['', Validators.required],
      categoryType: [0, Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.addCategoryForm.invalid) {
      return;
    }

    this.catSvc.addCategory(this.fControls.name.value, parseInt(this.fControls.categoryType.value, 2));
  }

}
