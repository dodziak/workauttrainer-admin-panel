import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  constructor(public catSvc: CategoriesService) {}

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    return this.catSvc.getCategories();
  }

  deleteCategory(id: string) {
    return this.catSvc.deleteCategory(id);
  }

  changePage(page: number) {
    this.catSvc.changePage(page);
  }

}
