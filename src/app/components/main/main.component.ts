import { JwtService } from 'src/app/services/jwt.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private jwtSvc: JwtService, private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.jwtSvc.logout();
    this.router.navigate(['/login']);
    console.log('logged out');
  }
}
