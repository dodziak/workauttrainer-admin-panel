import { JwtService } from './../../../services/jwt.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;

  // łatwy dostep do pól formularza
  get fControls() { return this.loginForm.controls; }

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private jwtSvc: JwtService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.jwtSvc.login(this.fControls.email.value, this.fControls.password.value).then(x => {
      console.log('login comp', x);
      this.router.navigate(['/dashboard']);
    });

  }

}
