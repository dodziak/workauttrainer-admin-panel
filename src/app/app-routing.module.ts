import { NewCategoryComponent } from './components/main/categories/new-category/new-category.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { DashboardComponent } from './components/main/dashboard/dashboard.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { MainComponent } from './components/main/main.component';
import { TrainingsComponent } from './components/main/trainings/trainings.component';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { CategoriesComponent } from './components/main/categories/categories.component';
import { ExercisesComponent } from './components/main/exercises/exercises.component';
import { CategoryListComponent } from './components/main/categories/category-list/category-list.component';
import { NewExerciseComponent } from './components/main/exercises/new-exercise/new-exercise.component';
import { ExercisesListComponent } from './components/main/exercises/exercises-list/exercises-list.component';
import { NewTrainingComponent } from './components/main/trainings/new-training/new-training.component';
import { TrainingsListComponent } from './components/main/trainings/trainings-list/trainings-list.component';


const routes: Routes = [
  { path: '', component: AppComponent },
  {
    path: 'auth', component: AuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
    ]
  },
  {
    path: 'main', component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'categories', component: CategoriesComponent,
        children: [
          { path: 'new', component: NewCategoryComponent },
          { path: 'list', component: CategoryListComponent }
        ]
      },
      {
        path: 'exercises', component: ExercisesComponent,
        children: [
          { path: 'new', component: NewExerciseComponent },
          { path: 'list', component: ExercisesListComponent }
        ]
      },
      {
        path: 'trainings', component: TrainingsComponent,
        children: [
          { path: 'new', component: NewTrainingComponent },
          { path: 'list', component: TrainingsListComponent }
        ]
      },
    ]
  },
  { path: '**', component: AppComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
