import { JwtService } from 'src/app/services/jwt.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'loginapp';

  constructor(private jwtSvc: JwtService, private router: Router) {

  }

  ngOnInit() {
    if (this.jwtSvc.loggedIn){
      this.router.navigate(['main/dashboard']);
    } else {
        this.router.navigate(['auth/login']);
    }
  }

}
